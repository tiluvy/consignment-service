module bitbucket.org/tiluvy/consignment-service

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.11.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/net v0.0.0-20191002035440-2ec189313ef0
)
